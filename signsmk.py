import cv2
import numpy as np
import math
from find_line import tuberia
import matplotlib.image as mpimg
import os
import tensorflow as tf
from utils import visualization_utils as vis_util
from utils import label_map_util
import pygame
import time

pathw = "/home/mkfd/PythonProjects/IA/COPILOTO-MK/frame_temp/frame.jpg"

tsrc = tuberia.src_alert
x_myr_a = tsrc[3][0]
x_mnr_a = tsrc[2][0]
x_mnr_ap = tsrc[1][0]
x_myr_ap = tsrc[0][0]
y_myr_a = tsrc[3][1]
y_mnr_a = tsrc[0][1]
areas_list = []
div = 5
oldtime = time.time()

if os.path.exists(pathw):
    os.remove(pathw)

########################################## CONFIGURACIONES DE MODELO ###################
# What model to archieve.
MODEL_NAME = 'ssd_mobilenet_v1_coco_2018_01_28'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph_sign.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = 'data/label_map_sign.pbtxt'

NUM_CLASSES = 90

MINIMUM_CONFIDENCE = 0.4

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
CATEGORY_INDEX = label_map_util.create_category_index(categories)


#########################

num_frame = 0
cap = cv2.VideoCapture("/home/mkfd/Vídeos/copiloto_test/seniales.mp4")
# cap = cv2.VideoCapture("/home/mkfd/Vídeos/copiloto_test/pro.mp4")
# cap = cv2.VideoCapture("/home/mkfd/Vídeos/copiloto_test/test_3.mp4")

################################################ DIVIDIR EL AREA EN RECTANGULOS MAS PEQUEÑOS
def lista_areas():
    y = y_myr_a - y_mnr_a
    x = x_myr_a - x_mnr_ap
    inter_y = y/div
    inter_x = x/div
    x_ = 0
    y_ = 0
    for i in range(0,div):
        y_ += inter_y
        areas_list.append([(x_mnr_a+x_), y_myr_a-y_, x_myr_a-x_, y_myr_a+inter_y-y_])
        x_ += inter_x
    print("fin")
    print(areas_list)


def timePassed(old):
    currenttime = time.time()
    if currenttime - old > 3:
        return True
    else:
        return False

pygame.mixer.init()
pygame.mixer.music.load("sound/alert.mp3")
###########################################333
def alerta(img,x1,y1,x2,y2):
    global oldtime
    for area in areas_list:
        # cv2.rectangle(img, (int(area[0]),int(area[1])),(int(area[2]),int(area[3])),(25,25,255),2)
        if ((x1>=area[0] and x1<=area[2]) and
        (y1>=area[1] and y1<=area[3])) or ((x2>=area[0] and x2<=area[2]) and
        (y2>=area[1] and y2<=area[3])):
            cv2.putText(img, "ALERTA", (50, 110), cv2.FONT_HERSHEY_DUPLEX, 1.9, (255, 0, 255), 2, cv2.LINE_AA)
            if not pygame.mixer.music.get_busy() and timePassed(oldtime):
                pygame.mixer.music.play()
                oldtime = time.time()
            break


lista_areas()
# Load model into memory
print('Cargando modelo...')
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.compat.v2.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')



with detection_graph.as_default():
    with tf.compat.v1.Session(graph=detection_graph) as sess:
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        while (cap.isOpened()):
            _, frame = cap.read()
            image_out = frame

            if not _:
                print("fin video")
                break
            if num_frame == 6:
                cv2.polylines(image_out, [np.int32(tsrc)], True, (255, 0, 0), 3)
                ######################DETECTAR CARRO
                # try:
                image_np_expanded = np.expand_dims(image_out, axis=0)

                (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

                if num>0:
                    # print("num: "+str(num))
                    boxes = np.squeeze(boxes)
                    scores = np.squeeze(scores)
                    #set a min thresh score, say 0.8
                    min_score_thresh = 0.5
                    bboxes = boxes[scores > min_score_thresh]

                    #get image size
                    im_width, im_height = (1280,720)
                    for box in bboxes:
                        ymin, xmin, ymax, xmax = box
                        # final_box.append([xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height])
                        x1 = int(xmin * im_width)
                        y1 = int( ymin * im_height)
                        x2 = int(xmax * im_width)
                        y2 = int( ymax * im_height)
                        # cv2.rectangle(image_out, (ix,iy),(x,y),(25,255,150),5)
                        alerta(image_out,x1,y1,x2,y2)


                # counter, csv_line, counting_mode
                counter, csv_line = vis_util.visualize_boxes_and_labels_on_image_array(cap.get(1),
                image=image_out,
                mode=2,
                color_recognition_status=0,
                boxes=np.squeeze(boxes),
                classes=np.squeeze(classes).astype(np.int32),
                scores=np.squeeze(scores),
                category_index=CATEGORY_INDEX,
                min_score_thresh=MINIMUM_CONFIDENCE,
                use_normalized_coordinates=True,
                y_reference = 50,
                deviation = 3,
                line_thickness=4)

                ###################FIN DETECTAR CARRO
                cv2.imwrite(pathw, image_out)
                imj = mpimg.imread(pathw)
                # ij = cv2.imread(pathw)
                # result = tuberia.pipeline(imj)
                cv2.imshow('result',image_out)
                os.remove(pathw)
                num_frame = 0


            num_frame = num_frame+1
            #cv2.imshow('resutl', canny_image)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                print("alg")
                print(tsrc)
                break

        cap.release()
        cv2.destroyAllWindows()
        sess.close()

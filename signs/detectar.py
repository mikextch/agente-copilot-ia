import warnings
warnings.filterwarnings('ignore')
import numpy as np
import os
import tensorflow as tf
from matplotlib import pyplot as plt
from PIL import Image
import glob as glob

import sys
# Append your Tensorflow object detection and darkflow directories to your path
sys.path.append('PATH_TO_TENSORFLOW_OBJECT_DETECTION_FOLDER') # ~/tensorflow/models/research/object_detection
sys.path.append('PATH_TO_DARKFLOW_FOLDER') # ~/darkflow
from utils import label_map_util
from utils import visualization_utils as vis_util

# Agente Copiloto

Agente inteligente que tiene como rol ser un copiloto, ayuda a detectar carriles, obstáculos, vehiculos,
y señales de transito.

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Podrá instalar todos los requerimiento a traves del archivo requirements.txt_

```
pip install -r requirements.txt
```
_Se sugiere usar entornos virtuales para instalar los requerimientos_


## Ejecutando las pruebas ⚙️

_Para la ejecución del proyecto ejecutar el archivo principal_
```
python copilotomk.py
```


## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Opencv](https://docs.opencv.org/4.1.2/) - Libreria de vision computacional

## Contribuyendo 🖇️

Por el momento no estamos aceptando contribuciones, pero trabajamos duro
para que pronto sea posible recibir su contribución. Gracias!


## Autores ✒️

_Solo lo poco que he contribuido_

* **Miguel Xet** - *Trabajo Inicial* - [Miguel Xet](https://gitlab.com/mikevi77)


## Licencia 📄
Este proyecto está bajo la Licencia

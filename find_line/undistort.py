import cv2
import numpy as np
import os
import pickle
import find_line.plot_images as plotimg

# Puntos para encerrar el area de interes de busqueda del carril
src = np.float32([
    (696,455),
    (587,455),
    (235,700),
    (1075,700)
])

src_alert = np.float32([
    (661,470),
    (622,470),
    (380,700),
    (900,700)
])
##############################################################################################
################################# CALIBRATION  ###############################################
def calibrate_camera():
    '''
    Calcula la matriz de calibración de la cámara y los coeficientes de distorsión.
    : return: matriz de calibración de la cámara y coeficientes de distorsión
    '''

    imgpaths = glob.glob('camera_cal/calibration*.jpg')
    sort_nicely(img_paths)

    # View a sample calibration image
    # %matplotlib inline

    image = cv2.imread(imgpaths[0])
    imshape = image.shape[:2] # gets only the (height, width) to be used in the cv2.calibrateCamera()

    plt.imshow(image)
    plt.show()
    print('Image shape: {}'.format(image.shape))

    # %matplotlib qt
    print()
    print('Calibrating the camera...')
    print()

    objpoints = []
    imgpoints = []

    nx = 9 # Number of inside corners on each row of the chessboard
    ny = 6 # Number of inside corners on each column of the chessboard

    # Prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros([ny*nx, 3], dtype=np.float32)
    objp[:,:2] = np.mgrid[0:nx, 0:ny].T.reshape(-1, 2)

    # Iterate over each calibration image and determine the objpoints and imgpoints
    for idx, imgpath in enumerate(imgpaths):
        img = cv2.imread(imgpath)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        ret, corners = cv2.findChessboardCorners(gray, (nx, ny), None)
        if ret:
            img = cv2.drawChessboardCorners(img, (nx, ny), corners, ret)

            imgpoints.append(corners)
            objpoints.append(objp)

            cv2.imshow('img', img)
            cv2.waitKey(500)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, imshape[::-1], None, None)

    print('Calibration complete!')
    cv2.destroyAllWindows()
    return mtx, dist


################################################# Arreglar distorcion de imagen
def undistort(img, mtx, dist):
    '''
    No distorsiona una imagen
     : param img (ndarray): Imagen, representada como una matriz numpy
     : param mtx: matriz de calibración de la cámara
     : param dist: Distorsión coeff's
     : volver: imagen no distorsionada
    '''

    undistort = cv2.undistort(img, mtx, dist, None, mtx)
    return undistort

# ####### Deformar imagen
def warp_image(img, warp_shape, src, dst):
    '''
    Realiza la transformación de perspectiva (PT)
     : param img (ndarray): imagen
     : param warp_shape: Forma de la imagen deformada
     : param src (ndarray): puntos de origen
     : param dst (ndarray): puntos de destino
     : return: Tuple (Imagen transformada, matriz PT, matriz inversa PT)
    '''

    # Obtenga la matriz de transformación de perspectiva y su inverso
    M = cv2.getPerspectiveTransform(src, dst)
    invM = cv2.getPerspectiveTransform(dst, src)

    # Deformar la imagen
    warped = cv2.warpPerspective(img, M, warp_shape, flags=cv2.INTER_LINEAR)
    return warped, M, invM

### ROI
def get_roi(img, vertices):
    '''
    Transforma una imagen conservando solo el ROI representado por el
     los 'vértices' y elimina el resto de la imagen al establecer la intensidad de píxeles en 0
     : param img (ndarray): imagen
     : param vertices (ndarray): Región de interés de la imagen
     : return: imagen modificada
    '''

    vertices = np.array(vertices, ndmin=3, dtype=np.int32)
    if len(img.shape) == 3:
        fill_color = (255,) * 3
    else:
        fill_color = 255

    mask = np.zeros_like(img)
    mask = cv2.fillPoly(mask, vertices, fill_color)
    return cv2.bitwise_and(img, mask)

def preprocess_image(img, visualise=False):
    '''
    Preprocesa una imagen. Los pasos incluyen:
     1. Corrección de distorsión
     2. Transformación de perspectiva
     3. Cultivo de ROI

     : param img (ndarray): Imagen original
     : param visualise (boolean): bandera booleana para visualización
     : retorno: imagen preprocesada, (matriz PT, matriz inversa PT)
    '''

    ysize = img.shape[0]
    xsize = img.shape[1]

    # 1. Corrección de distorsión
    undist = undistort(img, mtx, dist)

    # 2. Transformación de perspectiva
    # src = np.float32([
    #     (696,455),
    #     (587,455),
    #     (235,700),
    #     (1075,700)
    # ])

    dst = np.float32([
        (xsize - 350, 0),
        (350, 0),
        (350, ysize),
        (xsize - 350, ysize)
    ])

    warped, M, invM = warp_image(undist, (xsize, ysize), src, dst)

    # 3. ROI crop
    vertices = np.array([
        [200, ysize],
        [200, 0],
        [1100, 0],
        [1100, ysize]
    ])

    roi = get_roi(warped, vertices)

    # 4. Visualiza la transformación
    if visualise:
        img_copy = np.copy(img)
        roi_copy = np.copy(roi)

        cv2.polylines(img_copy, [np.int32(src)], True, (255, 0, 0), 3)
        cv2.polylines(roi_copy, [np.int32(dst)], True, (255, 0, 0), 3)

        plotimg.plot_images([
            (img_copy, 'Original Image'),
            (roi_copy, 'Bird\'s Eye View Perspective')
        ])

    return roi, (M, invM)


# Nota: el proceso de calibración solo necesita ejecutarse una vez en ausencia
# del archivo pickle que contiene los parámetros calculados anteriormente mencionados
if os.path.exists('camera_calib.p'):
    with open('camera_calib.p', mode='rb') as f:
        data = pickle.load(f)
        mtx, dist = data['mtx'], data['dist']
        print('¡Cargó la matriz de calibración de la cámara guardada y los coeficientes dist!')
else:
    mtx, dist = calibrate_camera()
    with open('camera_calib.p', mode='wb') as f:
        pickle.dump({'mtx': mtx, 'dist': dist}, f)

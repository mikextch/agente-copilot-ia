import cv2
import numpy as np
import find_line.poly_points as polyPoint #clase local

IMG_SHAPE = (720, 1280)
cache = np.array([])



########################
def check_validity(left_fit, right_fit, diagnostics=False):
    '''
    Determinar la validez de las líneas de carril representadas por un conjunto de coeficientes polinómicos de segundo orden.
     : param left_fit (ndarray): coeficientes para el polinomio de segundo orden que define la línea del carril izquierdo
     : param right_fit (ndarray): coeficientes para el polinomio de segundo orden que define la línea del carril derecho
     : param diagnostics (booleano): indicador booleano para el registro
     : return (booleano)
    '''

    if left_fit is None or right_fit is None:
        print("no left ni right")
        return False

    plot_xleft, plot_yleft, plot_xright, plot_yright = polyPoint.get_poly_points(left_fit, right_fit)

    # Compruebe si las dos líneas se encuentran dentro de una distancia
    # plosible entre sí para tres valores de y distintos

    y1 = IMG_SHAPE[0] - 1 # Bottom
    y2 = IMG_SHAPE[0] - int(min(len(plot_yleft), len(plot_yright)) * 0.35) # For the 2nd and 3rd, take values between y1 and the top-most available value.
    y3 = IMG_SHAPE[0] - int(min(len(plot_yleft), len(plot_yright)) * 0.75)

    # Compute the respective x-values for both lines
    x1l = left_fit[0]  * (y1**2) + left_fit[1]  * y1 + left_fit[2]
    x2l = left_fit[0]  * (y2**2) + left_fit[1]  * y2 + left_fit[2]
    x3l = left_fit[0]  * (y3**2) + left_fit[1]  * y3 + left_fit[2]

    x1r = right_fit[0] * (y1**2) + right_fit[1] * y1 + right_fit[2]
    x2r = right_fit[0] * (y2**2) + right_fit[1] * y2 + right_fit[2]
    x3r = right_fit[0] * (y3**2) + right_fit[1] * y3 + right_fit[2]

    # Calcule las normas L1
    x1_diff = abs(x1l - x1r)
    x2_diff = abs(x2l - x2r)
    x3_diff = abs(x3l - x3r)

    # Definir los valores umbral para cada uno de los tres puntos.
    min_dist_y1 = 480 # 510 # 530
    max_dist_y1 = 730 # 750 # 660
    min_dist_y2 = 280
    max_dist_y2 = 730 # 660
    min_dist_y3 = 140
    max_dist_y3 = 730 # 660

    if (x1_diff < min_dist_y1) | (x1_diff > max_dist_y1) | \
        (x2_diff < min_dist_y2) | (x2_diff > max_dist_y2) | \
        (x3_diff < min_dist_y3) | (x3_diff > max_dist_y3):
        if diagnostics:
            print("Criterio de distancia violado: " +
                  "x1_diff == {:.2f}, x2_diff == {:.2f}, x3_diff == {:.2f}".format(x1_diff, x2_diff, x3_diff))
        return False

    # Compruebe si las pendientes de la línea son similares para dos valores de y distintos
    # x = Ay**2 + By + C
    # dx/dy = 2Ay + B

    y1left_dx  = 2 * left_fit[0]  * y1 + left_fit[1]
    y3left_dx  = 2 * left_fit[0]  * y3 + left_fit[1]
    y1right_dx = 2 * right_fit[0] * y1 + right_fit[1]
    y3right_dx = 2 * right_fit[0] * y3 + right_fit[1]

    # Calcule la norma L1
    norm1 = abs(y1left_dx - y1right_dx)
    norm2 = abs(y3left_dx - y3right_dx)

#     if diagnostics: print( norm1, norm2)

    # Definir el umbral de la norma L1
    thresh = 0.6 #0.58
    if (norm1 >= thresh) | (norm2 >= thresh):
        if diagnostics:
            print("Criterio tangente violado: " +
                  "norm1 == {:.3f}, norm2 == {:.3f} (thresh == {}).".format(norm1, norm2, thresh))
            return False

    return True


##########################
def polyfit_sliding_window(binary, lane_width_px=578, visualise=False, diagnostics=False):
    '''
    Detecta líneas de carril en una imagen binaria con umbral utilizando la técnica de ventana deslizante
     : param binary (ndarray): imagen binaria con umbral
     : param lane_width_px (int): Ancho de línea de carril promedio (en px) para la imagen deformada
     calculado empíricamente
     : param visualise (boolean): bandera booleana para visualización
     : param diagnostics (boolean): indicador booleano para el registro
    '''

    global cache
    ret = True

    # Prueba de cordura
    if binary.max() <= 0:
        print("curdura fallada")
        return False, np.array([]), np.array([]), np.array([])

    # Paso 1: Calcule el histograma a lo largo de todas las columnas en la mitad inferior de la imagen.
    # Los dos picos más destacados en este histograma serán buenos indicadores
    # de la posición x de la base de las líneas de carril
    histogram = None
    cutoffs = [int(binary.shape[0] / 2), 0]

    for cutoff in cutoffs:
        histogram = np.sum(binary[cutoff:, :], axis=0)

        if histogram.max() > 0:
            break

    if histogram.max() == 0:
        print('No se pueden detectar líneas de carril en este marco. Intentando otro marco!')
        return False, np.array([]), np.array([])

    # Encuentra el pico de las mitades izquierda y derecha del histograma
    # Estos serán el punto de partida para las líneas izquierda y derecha
    midpoint = np.int(histogram.shape[0] / 2)
    leftx_base = np.argmax(histogram[:midpoint])
    rightx_base = np.argmax(histogram[midpoint:]) + midpoint

    if visualise:
        plot_images([(binary, 'Binary')])
        plt.plot(histogram, 'm', linewidth=4.0)
        plt.plot((midpoint, midpoint), (0, IMG_SHAPE[0]), 'c')
        plt.plot((0, IMG_SHAPE[1]), (cutoff, cutoff), 'c')

    out = np.dstack((binary, binary, binary)) * 255

    nb_windows = 12 # cantidad de ventanas correderas
    margin = 100 # ancho de las ventanas +/- margen
    minpix = 50 # Número mínimo de píxeles necesarios para volver a centrar la ventana
    window_height = int(IMG_SHAPE[0] / nb_windows)
    min_lane_pts = 10  # se necesita un número mínimo de píxeles 'calientes' para ajustar
                       #un polinomio de segundo orden como una línea de carril

    # Identifique las posiciones x-y de todos los píxeles distintos de cero en la imagen.
     # Nota: los índices aquí son equivalentes a las ubicaciones de coordenadas de
    # pixel
    nonzero = binary.nonzero()
    nonzerox = np.array(nonzero[1])
    nonzeroy = np.array(nonzero[0])

    # Posiciones actuales a actualizar para cada ventana
    leftx_current = leftx_base
    rightx_current = rightx_base

    # Listas vacías para recibir índices de píxeles del carril izquierdo y derecho
    left_lane_inds = []
    right_lane_inds = []

    for window in range(nb_windows):
        # Identificar los límites de la ventana en x e y (e izquierda y derecha)
        win_y_low = IMG_SHAPE[0] - (1 + window) * window_height
        win_y_high = IMG_SHAPE[0] - window * window_height

        win_xleft_low = leftx_current - margin
        win_xleft_high = leftx_current + margin

        win_xright_low = rightx_current - margin
        win_xright_high = rightx_current + margin

        # Dibujar ventanas para visualización
        cv2.rectangle(out, (win_xleft_low, win_y_low), (win_xleft_high, win_y_high),\
                      (0, 255, 0), 2)
        cv2.rectangle(out, (win_xright_low, win_y_low), (win_xright_high, win_y_high),\
                      (0, 255, 0), 2)

        # Identifique los píxeles distintos de cero en x e y dentro de la ventana
        good_left_inds = ((nonzeroy >= win_y_low) & (nonzeroy <= win_y_high)
                         & (nonzerox >= win_xleft_low) & (nonzerox <= win_xleft_high)).nonzero()[0]
        good_right_inds = ((nonzeroy >= win_y_low) & (nonzeroy <= win_y_high)
                         & (nonzerox >= win_xright_low) & (nonzerox <= win_xright_high)).nonzero()[0]

        left_lane_inds.append(good_left_inds)
        right_lane_inds.append(good_right_inds)

        # Si encontró > píxeles minpíxeles, vuelva a centrar la siguiente ventana en su posición media
        if len(good_left_inds) >  minpix:
            leftx_current = int(np.mean(nonzerox[good_left_inds]))

        if len(good_right_inds) > minpix:
            rightx_current = int(np.mean(nonzerox[good_right_inds]))

    left_lane_inds = np.concatenate(left_lane_inds)
    right_lane_inds = np.concatenate(right_lane_inds)

    # Extraer posiciones de píxeles para las líneas de carril izquierdo y derecho
    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds]
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]

    left_fit, right_fit = None, None

    # Prueba de cordura; Ajuste un polinomio de segundo orden para cada píxel de línea de carril
    if len(leftx) >= min_lane_pts and len(rightx) >= min_lane_pts:
        print("cordura pixel")
        left_fit = np.polyfit(lefty, leftx, 2)
        right_fit = np.polyfit(righty, rightx, 2)

    # Validar líneas de carril detectadas
    valid = check_validity(left_fit, right_fit, diagnostics=diagnostics)

    if not valid:
        # Si las líneas de carril detectadas NO son válidas:
        # 1. Calcule las líneas de carril como un promedio de las líneas detectadas previamente
            # de la memoria caché y marque este ciclo de detección como un error estableciendo ret = False
        # 2. De lo contrario, si el caché está vacío, regrese

        if len(cache) == 0:
            if diagnostics: print('ADVERTENCIA: No se pueden detectar líneas de carril en este cuadro.')
            return False, np.array([]), np.array([])

        avg_params = np.mean(cache, axis=0)
        left_fit, right_fit = avg_params[0], avg_params[1]
        ret = False

    plot_xleft, plot_yleft, plot_xright, plot_yright = polyPoint.get_poly_points(left_fit, right_fit)

    # Colorea los píxeles detectados para cada línea de carril
    out[nonzeroy[left_lane_inds], nonzerox[left_lane_inds]] = [255, 0, 0]
    out[nonzeroy[right_lane_inds], nonzerox[right_lane_inds]] = [255, 10, 255]

    left_poly_pts = np.array([np.transpose(np.vstack([plot_xleft, plot_yleft]))])
    right_poly_pts = np.array([np.transpose(np.vstack([plot_xright, plot_yright]))])

    # Trazar el polinomio ajustado
    cv2.polylines(out, np.int32([left_poly_pts]), isClosed=False, color=(200,255,155), thickness=4)
    cv2.polylines(out, np.int32([right_poly_pts]), isClosed=False, color=(200,255,155), thickness=4)

    if visualise:
        plot_images([(img, 'Original'), (out, 'Out')], figsize=(30, 40))

    return ret, out, np.array([left_fit, right_fit])

import numpy as np

IMG_SHAPE = (720, 1280)
######################3
def get_poly_points(left_fit, right_fit):
    '''
    Obtenga los puntos para el carril izquierdo / carril derecho definidos por el 'left_fit' del coeff polinomial
     y 'right_fit'
     : param left_fit (ndarray): coeficientes para el polinomio que define la línea del carril izquierdo
     : param right_fit (ndarray): coeficientes para el polinomio que define la línea del carril derecho
     : return (Tuple (ndarray, ndarray, ndarray, ndarray)): coordenadas x-y para las líneas de carril izquierdo y derecho
    '''
    ysize, xsize = IMG_SHAPE

    # Get the points for the entire height of the image
    plot_y = np.linspace(0, ysize-1, ysize)
    plot_xleft = left_fit[0] * plot_y**2 + left_fit[1] * plot_y + left_fit[2]
    plot_xright = right_fit[0] * plot_y**2 + right_fit[1] * plot_y + right_fit[2]

    # But keep only those points that lie within the image
    plot_xleft = plot_xleft[(plot_xleft >= 0) & (plot_xleft <= xsize - 1)]
    plot_xright = plot_xright[(plot_xright >= 0) & (plot_xright <= xsize - 1)]
    plot_yleft = np.linspace(ysize - len(plot_xleft), ysize - 1, len(plot_xleft))
    plot_yright = np.linspace(ysize - len(plot_xright), ysize - 1, len(plot_xright))

    return plot_xleft.astype(np.int), plot_yleft.astype(np.int), plot_xright.astype(np.int), plot_yright.astype(np.int)

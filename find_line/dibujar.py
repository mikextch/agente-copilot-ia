import cv2
import numpy as np
from find_line.poly_points import get_poly_points
from find_line.binary_image import get_binary_image
import find_line.undistort as undit
import matplotlib.image as mpimg
import glob
import re

################ METODOS DE AYUDA
def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    '''
    Turn a string into a list of string and number chunks.
    E.g. "z23a" -> ["z", 23, "a"]
    '''
    return [tryint(c) for c in re.split('([0-9]+)', s)]

def sort_nicely(l):
    '''
    Sort the given list in the way that humans expect.
    '''
    l.sort(key=alphanum_key)

##############################################################################3

def compute_mppx(img, dashed_line_loc, visualise=False):
    '''
    Convierte del espacio de píxeles al espacio del mundo real y calcula los metros / píxel
     : param img (ndarray): Imagen deformada
     : param dashed_line_loc (string): ubicación de línea discontinua (izquierda / derecha)
     : param visualize (boolean): bandera booleana para visualización
     : return (float32, float32): (metros / pixel y dirección, metros / pixel x-direction)
    '''
    lane_width = 3.7
    dashed_line_len = 3.048

    if dashed_line_loc == 'left':
        y_top = 295
        y_bottom = 405
    elif dashed_line_loc == 'right':
        y_top = 395
        y_bottom = 495

    binary = get_binary_image(img)
    histogram = np.sum(binary[int(binary.shape[0] / 2):, :], axis=0)
    midpoint = np.int(histogram.shape[0] / 2)

    x_left = np.argmax(histogram[:midpoint])
    x_right = np.argmax(histogram[midpoint:]) + midpoint

    x_mppx = lane_width / (x_right - x_left)
    y_mppx = dashed_line_len / (y_bottom - y_top)

    if visualise:
        plt.figure(figsize=(10, 8))
        plt.imshow(img)
        plt.axis('off')

        if dashed_line_loc == 'left':
            plt.plot((x_left, x_left), (y_top, y_bottom), 'r')
            plt.text(x_left + 10, (y_top + y_bottom) // 2, '{} m'.format(dashed_line_len), color='r', fontsize=20)

        elif dashed_line_loc == 'right':
            plt.plot((x_right, x_right), (y_top, y_bottom), 'r')
            plt.text(x_right + 10, (y_top + y_bottom) // 2, '{} m'.format(dashed_line_len), color='r',fontsize=20)

        plt.plot((x_left, x_right), (img.shape[0] - 200 , img.shape[0] - 200), 'r')
        plt.text((x_left + x_right) // 2, img.shape[0] - 220, '{} m'.format(lane_width), color='r', fontsize=20)

    return y_mppx, x_mppx



def draw(img, warped, invM, poly_param, curve_rad, offset):
    '''
    Función de utilidad para dibujar los límites del carril y la estimación numérica de la curvatura del carril y la posición del vehículo.
     : param img (ndarray): imagen original
     : param warped (ndarray): imagen deformada
     : param invM (ndarray): matriz de transformación perpsectiva inversa
     : param poly_param (ndarray): conjunto de coeficientes polinomiales de segundo orden que representan las líneas de carril detectadas
     : param curve_rad (float32): curvatura de línea de carril
     : compensación de parámetro (float32): compensación de automóvil
     : return (ndarray): imagen con visualización
    '''

    undist = undit.undistort(img, undit.mtx, undit.dist)
    warp_zero = np.zeros_like(warped[:,:,0]).astype(np.uint8)
    color_warp = np.dstack((warp_zero, warp_zero, warp_zero))

    left_fit = poly_param[0]
    right_fit = poly_param[1]
    plot_xleft, plot_yleft, plot_xright, plot_yright = get_poly_points(left_fit, right_fit)

    pts_left = np.array([np.transpose(np.vstack([plot_xleft, plot_yleft]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([plot_xright, plot_yright])))])
    pts = np.hstack((pts_left, pts_right))

    # Color the road
    cv2.fillPoly(color_warp, np.int_([pts]), (0, 220, 110))

    cv2.polylines(color_warp, np.int32([pts_left]), isClosed=False,
                  color=(255, 255, 255), thickness=10)
    cv2.polylines(color_warp, np.int32([pts_right]), isClosed=False,
                  color=(255, 255, 255), thickness= 10)

    # Unwarp and merge with undistorted original image
    unwarped = cv2.warpPerspective(color_warp, invM, (img.shape[1], img.shape[0]), flags=cv2.INTER_LINEAR)
    out = cv2.addWeighted(undist, 1, unwarped, 0.4, 0)

    # Write data on the image
    if (left_fit[1] + right_fit[1]) / 2 > 0.05:
        # text = 'Curvatura izquierda, c radius: {:04.2f} m'.format(curve_rad)
        text = 'Curvatura izquierda'
    elif (left_fit[1] + right_fit[1]) / 2 < -0.05:
        # text = 'Curvatura derecha, c radius: {:04.2f} m'.format(curve_rad)
        text = 'Curvatura derecha'
    else:
        text = 'Recta'

    cv2.putText(out, text, (50, 60), cv2.FONT_HERSHEY_DUPLEX, 1.2, (255, 255, 255), 2, cv2.LINE_AA)

    direction = ''
    if offset > 0:
        direction = 'derecha'
    elif offset < 0:
        direction = 'izquierda'

    text = '{:0.1f} cm {} del centro'.format(abs(offset) * 100, direction)
    cv2.putText(out, text, (50, 110), cv2.FONT_HERSHEY_DUPLEX, 1.2, (255, 255, 255), 2, cv2.LINE_AA)

    return out


def get_image(img_path, visualise=False):
    '''
    Cargue una imagen desde 'img_path' y preprocese
     : param img_path (string): ruta de la imagen
     : param visualize (boolean): bandera booleana para visualización
     : return: Imagen transformada, (matriz PT, matriz PT inv)
    '''
    img = mpimg.imread(img_path)
    return undit.preprocess_image(img, visualise=visualise)


###############################################################
###############################################################

test_img_paths = glob.glob('/home/mkfd/PythonProjects/IA/copiloto-CNN/test_images_mk/*.jpg')
sort_nicely(test_img_paths)
###
visualise = False


img, _ = get_image(test_img_paths[0])
y_mppx1, x_mppx1 = compute_mppx(img, dashed_line_loc='right', visualise=visualise)

img, _ = get_image(test_img_paths[1])
y_mppx2, x_mppx2 = compute_mppx(img, dashed_line_loc='left', visualise=visualise)

x_mppx = (x_mppx1 + x_mppx2) / 2
y_mppx = (y_mppx1 + y_mppx2) / 2

print('Average meter/px along x-axis: {:.4f}'.format(x_mppx))
print('Average meter/px along y-axis: {:.4f}'.format(y_mppx))

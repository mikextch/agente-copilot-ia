import cv2
import numpy as np
import math
from find_line import undistort #Clase local
import find_line.binary_image as binaryImg #Clase local
from find_line import dibujar
import find_line.polyfit_window as polyWin #clase local
import find_line.adaptative_search as adapSearch #clase local
from find_line.curvatura import compute_offset_from_center, compute_curvature

reset = True
cache = np.array([])
attempts = 0
src = undistort.src
src_alert = undistort.src_alert

def setAllCache(cache):
    polyWin.cache = cache
    adapSearch.cache = cache

def setAllAttempst(attempt):
    adapSearch.attempts = attempt


def getAllCache(where):
    global cache
    if where == 1:
        cache = polyWin.cache
    elif where == 2:
        cache = adapSearch.cache

def pipeline(img, visualise=False, diagnostics=False):
    global cache
    global poly_param # Important for successive calls to the pipeline
    global attempts
    global reset
    max_attempts = 5

    result = np.copy(img)
    warped, (M, invM) = undistort.preprocess_image(img)
    title = ''
    # return warped
    try:
        if reset == True:
            title = 'Sliding window'
            if diagnostics: print(title)

            binary = binaryImg.get_binary_image(warped)
            ret, img_poly, poly_param = polyWin.polyfit_sliding_window(binary, diagnostics=diagnostics)
            getAllCache(1)
            if ret:
                if diagnostics: print('Success!')
                reset = False
                cache = np.array([poly_param])
                setAllCache(cache)
                # print("en cachee")
            else:
                if len(img_poly) == 0:
                    print('Sliding window failed!')
                    return img
        else:
            title = 'Adaptive Search'
            if diagnostics: print(title)

            img_poly, poly_param = adapSearch.polyfit_adapt_search(warped, poly_param, diagnostics=diagnostics)
            attempts = adapSearch.attempts
            getAllCache(2)
            if attempts == max_attempts:
                if diagnostics: print('Resetting...')
                reset = True
                attempts = 0
                setAllAttempst(attempts)

        left_curverad, right_curverad = compute_curvature(poly_param, dibujar.y_mppx, dibujar.x_mppx)
        offset = compute_offset_from_center(poly_param, dibujar.x_mppx)
        result = dibujar.draw(img, warped, invM, poly_param, (left_curverad + right_curverad) / 2, offset)
        # return result
        blended_warped_poly = cv2.addWeighted(img_poly, 0.6, warped, 1, 0)
        # ret2 = np.hstack([img_poly, blended_warped_poly])
        # ret3 = np.hstack([result, warped])
#         ret3 = triple_split_view([result, img_poly, blended_warped_poly])
        # ret3 = np.vstack([ret3, ret2])
        if visualise:
            plt.figure(figsize=(20, 12))
            plt.title(title)
            plt.imshow(ret3)

        return result

    except Exception as e:
        print(e)
        return img

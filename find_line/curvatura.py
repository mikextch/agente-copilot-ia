import cv2
import numpy as np
from find_line.poly_points import get_poly_points

IMG_SHAPE = (720, 1280)


def compute_offset_from_center(poly_param, x_mppx):
    '''
    Calcula el desplazamiento del automóvil desde el centro de las líneas de carril detectadas
     : param poly_param (ndarray): conjunto de coeficientes polinomiales de segundo orden que representan las líneas de carril detectadas
     : param x_mppx (float32): metros / píxel en la dirección x
     : return (float32): Offset
    '''
    plot_xleft, plot_yleft, plot_xright, plot_yright = get_poly_points(poly_param[0], poly_param[1])

    lane_center = (plot_xright[-1] + plot_xleft[-1]) / 2
    car_center = IMG_SHAPE[1] / 2

    offset = (lane_center - car_center) * x_mppx
    return offset

def compute_curvature(poly_param, y_mppx, x_mppx):
    '''
    Calcula la curvatura de las líneas del carril (en metros)
     : param poly_param (ndarray): conjunto de coeficientes polinomiales de segundo orden que representan las líneas de carril detectadas
     : param y_mppx (float32): metros / píxel en la dirección y
     : param x_mppx (float32): metros / píxel en la dirección x
     : return (float32): Curvatura (en metros)
    '''
    plot_xleft, plot_yleft, plot_xright, plot_yright = get_poly_points(poly_param[0], poly_param[1])

    y_eval = np.max(plot_yleft)

    left_fit_cr = np.polyfit(plot_yleft * y_mppx, plot_xleft * x_mppx, 2)
    right_fit_cr = np.polyfit(plot_yright * y_mppx, plot_xright * x_mppx, 2)

    left_curverad = ((1 + (2*left_fit_cr[0]* y_eval*y_mppx + left_fit_cr[1])**2)**1.5) / np.absolute(2*left_fit_cr[0])
    right_curverad = ((1 + (2*right_fit_cr[0]*y_eval*y_mppx + right_fit_cr[1])**2)**1.5) / np.absolute(2*right_fit_cr[0])

    return left_curverad, right_curverad

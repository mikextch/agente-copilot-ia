import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import math

def plot_images(data, layout='row', cols=2, figsize=(20, 12)):
    '''
    Función de utilidad para trazar imágenes
     : param data [(ndarray, string)]: Lista de datos para mostrar, [(imagen, título)]
     : param layout (string): diseño, en fila o en columna
     : param cols (number): número de columnas por fila
     : param figsize (number, number): Tupla que indica el tamaño de la figura
    '''
    rows = math.ceil(len(data) / cols)
    f, ax = plt.subplots(figsize=figsize)
    if layout == 'row':
        for idx, d in enumerate(data):
            img, title = d

            plt.subplot(rows, cols, idx+1)
            plt.title(title, fontsize=20)
            plt.axis('off')
            if len(img.shape) == 2:
                plt.imshow(img, cmap='gray')

            elif len(img.shape) == 3:
                plt.imshow(img)

    elif layout == 'col':
        counter = 0
        for r in range(rows):
            for c in range(cols):
                img, title = data[r + rows*c]
                nb_channels = len(img.shape)

                plt.subplot(rows, cols, counter+1)
                plt.title(title, fontsize=20)
                plt.axis('off')
                if len(img.shape) == 2:
                    plt.imshow(img, cmap='gray')

                elif len(img.shape) == 3:
                    plt.imshow(img)

                counter += 1

    return ax
